let eventChange = false;
let lastIdOpened = 0;
let myEvents = [];
let mySections = [];
let footerTranslated;
let myChips = [];
let currentLang = 'pt';
let translations = [];

document.querySelector('.timeline-hover').addEventListener('mouseenter', function() {
	document.querySelector('.timeline-tooltip').classList.replace('hide', 'show');
});

document.querySelector('.timeline-hover').addEventListener('mouseleave', function() {
	document.querySelector('.timeline-tooltip').classList.replace('show', 'hide');
});

document.querySelector('.attributes-hover').addEventListener('mouseenter', function() {
	document.querySelector('.attributes-tooltip').classList.replace('hide', 'show');
});

document.querySelector('.attributes-hover').addEventListener('mouseleave', function() {
	document.querySelector('.attributes-tooltip').classList.replace('show', 'hide');
});

document.querySelector('.events-hover').addEventListener('mouseenter', function() {
	document.querySelector('.event-tooltip').classList.replace('hide', 'show');
});

document.querySelector('.events-hover').addEventListener('mouseleave', function() {
	document.querySelector('.event-tooltip').classList.replace('show', 'hide');
});

function clearEventsHtml() {
	document.querySelector('.aside-timeline .content .events').innerHTML = '';
}

function addEventsToHtml() {
	myEvents.forEach(function(event, index) {
		let parentDiv = document.createElement('div');
		parentDiv.classList.add('event');

		if (event.type) {
			let dateDiv = document.createElement('div');
			dateDiv.classList.add('event-date');
			dateDiv.innerHTML = event.date;
			let circleDivContainer = document.createElement('div');
			circleDivContainer.classList.add('event-circle');
			let circleDiv = document.createElement('div');
			circleDiv.classList.add('circle');
			circleDiv.setAttribute('id', index);
			let titleDiv = document.createElement('div');
			titleDiv.classList.add('event-title');
			titleDiv.innerHTML = event.event;

			//append all tags
			parentDiv.appendChild(dateDiv);
			circleDivContainer.appendChild(circleDiv);
			parentDiv.appendChild(circleDivContainer);
			parentDiv.appendChild(titleDiv);

			//add events to the circle
			circleDiv.addEventListener('click', function() {
				if (
					(parentDiv.className.includes('personal') ||
						parentDiv.className.includes('educational') ||
						parentDiv.className.includes('professional')) &&
					!eventChange &&
					!parentDiv.className.includes('current')
				) {
					changeIndex(index);
				}
			});

			parentDiv.addEventListener('click', function() {
				if (
					(parentDiv.className.includes('personal') ||
						parentDiv.className.includes('educational') ||
						parentDiv.className.includes('professional')) &&
					!eventChange &&
					!parentDiv.className.includes('current') &&
					!parentDiv.className.includes('next')
				) {
					changeIndex(index);
				}
			});

			parentDiv.addEventListener('mouseenter', function() {
				if (
					(parentDiv.className.includes('personal') ||
						parentDiv.className.includes('educational') ||
						parentDiv.className.includes('professional')) &&
					!parentDiv.className.includes('current')
				) {
					parentDiv.classList.add('hovered');
				}
			});

			parentDiv.addEventListener('mouseleave', function() {
				parentDiv.classList.remove('hovered');
			});
		}
		document.querySelector('.aside-timeline .content .events').appendChild(parentDiv);
	});
}

function changeIndex(id) {
	eventChange = true;

	cleanChipStatus();
	document.querySelector('.card-main').classList.add('changing');
	let current = document.querySelectorAll('.event.current');
	if (current.length != 0) {
		current.forEach(function(curr) {
			curr.classList.remove('current');
		});
	}
	setTimeout(function() {
		document.querySelector('.card-main').classList.remove('changing');
		let newEvent = document.getElementById(id).parentElement.parentElement;
		newEvent.classList.remove('next');
		newEvent.classList.add('current');
		newEvent.classList.add(myEvents[id].type);
		// myEvents[id].opened = true;
		if (id > lastIdOpened) lastIdOpened = id;
		displayEvent(id);
		checkDuplicatedChips(id);
		nextIndex();
		checkControllerStatus();
	}, 500);
}

function nextIndex(force) {
	let nextId = myEvents.findIndex(function(item, index) {
		if (index > lastIdOpened && item.type) return index;
	});
	if (force != null) nextId = force;
	// console.log('next id:', nextId);
	if (nextId != -1) {
		let nextEvent = document.getElementById(nextId).parentElement.parentElement;
		// console.log(nextEvent);
		nextEvent.classList.add('next');
		nextEvent.classList.add(myEvents[nextId].type);
	}
}

function displayEvent(id) {
	let card = document.querySelector('.card-main');
	card.setAttribute('card', id);
	let cardHeader = document.querySelector('.card-main .event-header');
	let cardTitle = document.querySelector('.card-main .event-header h2');
	let cardDateTitle = document.querySelector('.card-main .event-header .date-tag');
	let cardIcon = document.querySelector('.card-main .event-header .icon-box i');

	cardHeader.classList.forEach(function(item) {
		if (item != 'event-header') cardHeader.classList.remove(item);
	});
	cardHeader.classList.add(myEvents[id].type);
	cardTitle.innerText = myEvents[id].event;
	cardDateTitle.innerHTML = myEvents[id].date;
	if (myEvents[id].type === 'personal') {
		cardIcon.innerHTML = 'person';
	} else if (myEvents[id].type === 'educational') {
		cardIcon.innerHTML = 'school';
	} else if (myEvents[id].type === 'professional') {
		cardIcon.innerHTML = 'work';
	}

	refreshCardContent(id);
}

function checkControllerStatus() {
	let eventList = document.querySelectorAll('.event');
	let possibleEvents = [];
	let currentIndex;
	eventList.forEach(function(event, index) {
		if (event.childElementCount > 0) {
			possibleEvents.push(index);
		}
		if (event.className.includes('current')) {
			currentIndex = index;
		}
	});
	if (currentIndex === possibleEvents[0]) {
		document.querySelector('.section-main .content .icons-controller .prev').classList.add('disabled');
		document.querySelector('.section-main .content .icons-controller .next').classList.remove('disabled');
	} else if (currentIndex === possibleEvents[possibleEvents.length - 1]) {
		document.querySelector('.section-main .content .icons-controller .next').classList.add('disabled');
		document.querySelector('.section-main .content .icons-controller .prev').classList.remove('disabled');
	} else {
		document.querySelector('.section-main .content .icons-controller .prev').classList.remove('disabled');
		document.querySelector('.section-main .content .icons-controller .next').classList.remove('disabled');
	}
}

function cleanChipStatus() {
	document.querySelectorAll('.attribute').forEach(function(tag) {
		tag.classList.remove('active');
	});
}

function focusAllChips() {
	document.querySelectorAll('.attribute').forEach(function(tag) {
		tag.classList.add('active');
	});
	document.querySelector('.filter-icon').classList.replace('hide', 'show');
}

function focusChip(chip) {
	document.querySelectorAll('.attribute').forEach(function(tag) {
		if (tag.children[0].innerHTML === chip) tag.classList.add('active');
	});
}

function addChip(newChip, chipType) {
	let targetDiv = document.querySelector('.attributes-list');
	let attributeTag = document.createElement('div');
	attributeTag.classList.add('attribute');
	let chipTag = document.createElement('span');
	chipTag.classList.add('attribute-text', chipType);
	chipTag.innerHTML = newChip;
	attributeTag.appendChild(chipTag);
	targetDiv.appendChild(attributeTag);
}

function checkDuplicatedChips(id) {
	//cleanChipStatus(); //this function will be triggered earlier
	if (myEvents[id].attributes) {
		myEvents[id].attributes.forEach(function(att, index) {
			setTimeout(function() {
				let duplicatedChip = false;
				myChips.forEach(function(chip) {
					if (att === chip.title) {
						duplicatedChip = true;
					}
				});
				if (!duplicatedChip) {
					addChip(att, myEvents[id].type);
					myChips.push({
						type: myEvents[id].type,
						title: att
					});
				}
				focusChip(att);
			}, index * 250);
		});
		setTimeout(function() {
			eventChange = false;
		}, myEvents[id].attributes.length * 250);
	} else if (myEvents[id].date === 'Futuro' || myEvents[id].date === 'Future') {
		focusAllChips();
		eventChange = false;
	}
}

function toggleFilters(status) {
	if (status) {
		document.querySelectorAll('.filter-options').forEach(function(item) {
			item.classList.replace('hide', 'show');
		});
		document.querySelector('.filter-icon').classList.add('opened');
		document.querySelector('.filter-icon').children[0].innerHTML = 'redo';
	} else {
		document.querySelectorAll('.filter-options').forEach(function(item) {
			item.classList.replace('show', 'hide');
		});
		document.querySelector('.filter-icon').classList.remove('opened');
		document.querySelector('.filter-icon').children[0].innerHTML = 'filter_alt';
	}
}

function clearChips() {
	document.querySelectorAll('.attribute').forEach(function(att) {
		att.remove();
	});
}

function addFilter(filter) {
	clearChips();
	let chipFilteredArray = myChips.filter(function(chip) {
		return chip.type === filter;
	});
	chipFilteredArray.forEach(function(chip, index) {
		setTimeout(function() {
			addChip(chip.title, chip.type);
			if (index === chipFilteredArray.length - 1) {
				checkDuplicatedChips(Number(document.querySelector('.card-main').getAttribute('card')));
				eventChange = false;
			}
		}, index * 100);
	});
}

function clearFilters() {
	document.querySelector('.filter-icon').classList.remove('disabled');
	document.querySelectorAll('.filter-options').forEach(function(filter) {
		filter.classList.remove('filtering');
	});
}

function addAllChips() {
	myChips.forEach(function(chip, index) {
		setTimeout(function() {
			addChip(chip.title, chip.type);
			if (index === myChips.length - 1) {
				checkDuplicatedChips(Number(document.querySelector('.card-main').getAttribute('card')));
				eventChange = false;
			}
		}, index * 100);
	});
}

function refreshCardContent(id) {
	let cardContent = document.querySelector('.card .event-content');
	cardContent.innerHTML = '';

	myEvents[id].title.forEach(function(item) {
		let divTag = document.createElement('div');
		divTag.classList.add('sub-content');
		let h3Tag = document.createElement('h3');
		h3Tag.innerHTML = item.first;
		let pTag = document.createElement('p');
		pTag.innerHTML = item.second;
		divTag.appendChild(h3Tag);
		divTag.appendChild(pTag);
		cardContent.appendChild(divTag);
	});
	let divTagDescription = document.createElement('div');
	divTagDescription.classList.add('sub-content');
	let pTagDescription = document.createElement('p');
	pTagDescription.classList.add('resume');
	pTagDescription.innerHTML = myEvents[id].description;
	divTagDescription.appendChild(pTagDescription);
	cardContent.appendChild(divTagDescription);
}

document.querySelector('.section-main .content .icons-controller .prev').addEventListener('click', function() {
	if (!this.className.includes('disabled') && !eventChange) {
		let prevEventId = 0;
		document.querySelectorAll('.event').forEach(function(tag) {
			if (tag.className.includes('current')) {
				changeIndex(prevEventId);
			} else if (tag.childElementCount > 0) {
				prevEventId = Number(tag.children[1].firstChild.id);
			}
		});
	}
});

document.querySelector('.section-main .content .icons-controller .next').addEventListener('click', function() {
	if (!this.className.includes('disabled') && !eventChange) {
		let nextEvent = false;
		document.querySelectorAll('.event').forEach(function(tag) {
			if (tag.className.includes('current')) {
				nextEvent = true;
			} else if (nextEvent && tag.childElementCount > 0) {
				changeIndex(Number(tag.children[1].firstChild.id));
				nextEvent = false;
			}
		});
	}
});

document.querySelector('.filter-icon').addEventListener('click', function() {
	if (this.className.includes('hide') || this.className.includes('disabled')) {
	} else {
		if (this.className.includes('opened')) toggleFilters(false);
		else toggleFilters(true);
	}
});

function checkIconsStatuses() {
	document.querySelectorAll('.filter-options').forEach(function(filter) {
		let type = filter.children[0].classList[1];
		if (type === 'personal') type = 'person';
		else if (type === 'educational') type = 'school';
		else if (type === 'professional') type = 'work';
		if (filter.className.includes('filtering')) {
			filter.children[0].innerHTML = 'close';
		} else {
			filter.children[0].innerHTML = type;
		}
	});
}

document.querySelectorAll('.filter-options').forEach(function(filter) {
	let type = filter.children[0].classList[1];
	if (type === 'personal') type = 'person';
	else if (type === 'educational') type = 'school';
	else if (type === 'professional') type = 'work';
	filter.addEventListener('click', function() {
		if (!eventChange) {
			if (!filter.className.includes('hide')) {
				eventChange = true;
				if (!filter.className.includes('filtering')) {
					clearFilters();
					filter.classList.add('filtering');
					document.querySelector('.filter-icon').classList.add('disabled');
					addFilter(filter.children[0].classList[1]);
				} else {
					clearFilters();
					clearChips();
					addAllChips();
				}
			}
			checkIconsStatuses();
		}
	});
});

document.querySelectorAll('.filter-options').forEach(function(filter) {
	filter.addEventListener('mouseenter', function() {
		checkIconsStatuses();
	});
	filter.addEventListener('mouseleave', function() {
		checkIconsStatuses();
	});
});

function translate(lang) {
	currentLang = lang;
	let classChange = '#' + lang + '-lang';
	document.querySelector('.lang-selected').classList.remove('lang-selected');
	document.querySelector(classChange).classList.add('lang-selected');
	if (lang === 'uk') {
		myEvents = translations.eventsEN;
		mySections = translations.sectionsEN;
		footerTranslated = translations.footerEN;
	} else if (lang === 'pt') {
		myEvents = translations.eventsPT;
		mySections = translations.sectionsPT;
		footerTranslated = translations.footerPT;
	}

	//tooltips & titles
	//First section
	document.querySelector('.aside-timeline .timeline-hover h2').innerHTML = mySections[0].title;
	document.querySelectorAll('.aside-timeline .timeline-tooltip .info-title').forEach(function(item, index) {
		item.innerHTML = mySections[0].tooltip.title[index];
	});
	document.querySelector('.aside-timeline .timeline-tooltip .personal').innerHTML = mySections[0].tooltip.personal;
	document.querySelector('.aside-timeline .timeline-tooltip .educational').innerHTML =
		mySections[0].tooltip.educational;
	document.querySelector('.aside-timeline .timeline-tooltip .professional').innerHTML =
		mySections[0].tooltip.professional;

	//Second section
	document.querySelector('.main-section-header .events-hover h2').innerHTML = mySections[1].title;
	document.querySelector('.main-section-header .event-tooltip .info-title').innerHTML =
		mySections[1].tooltip.title[0];

	//Third section
	document.querySelector('.aside-attributes .attributes-hover h2').innerHTML = mySections[2].title;
	document.querySelectorAll('.aside-attributes .attributes-tooltip .info-title').forEach(function(item, index) {
		item.innerHTML = mySections[2].tooltip.title[index];
	});
	document.querySelector(
		'.aside-attributes .attributes-tooltip .personal'
	).innerHTML = `<i class="material-icons personal">person</i>${mySections[2].tooltip.personal}`;
	document.querySelector(
		'.aside-attributes .attributes-tooltip .educational'
	).innerHTML = `<i class="material-icons educational">school</i>${mySections[2].tooltip.educational}`;
	document.querySelector(
		'.aside-attributes .attributes-tooltip .professional'
	).innerHTML = `<i class="material-icons professional">work</i>${mySections[2].tooltip.professional}`;

	//timeline
	document.querySelectorAll('.event').forEach(function(evt, index) {
		if (evt.childElementCount != 0) {
			evt.children[0].innerHTML = myEvents[index].date;
			evt.children[2].innerHTML = myEvents[index].event;
		}
	});

	//events
	let currentID = document.querySelector('.card-main').getAttribute('card');
	displayEvent(currentID);

	//attributes
	//checking filters
	let filteringType = 'all';
	document.querySelectorAll('.filter-options').forEach(function(filter) {
		if (filter.className.includes('filtering')) {
			filteringType = filter.children[0].classList[1];
		}
	});
	//applying translations to attributes
	let newAttrs = [];
	let allNewAttrs = [];
	if (filteringType === 'all' && document.querySelectorAll('.attribute-text').length > 0) {
		myEvents.forEach(function(item) {
			if (item.type && item.attributes) {
				item.attributes.forEach(function(att) {
					newAttrs.push(att);
					allNewAttrs.push(att);
				});
			}
		});
	} else if (filteringType != 'all' && document.querySelectorAll('.attribute-text').length > 0) {
		myEvents.forEach(function(item) {
			if (item.type === filteringType && item.attributes) {
				item.attributes.forEach(function(att) {
					newAttrs.push(att);
					allNewAttrs.push(att);
				});
			} else if (item.type && item.attributes) {
				item.attributes.forEach(function(att) {
					allNewAttrs.push(att);
				});
			}
		});
	}
	myChips.forEach(function(item, index) {
		item.title = allNewAttrs[index];
	});

	document.querySelectorAll('.attribute-text').forEach(function(chip, index) {
		chip.innerText = newAttrs[index];
	});

	//footer content
	let footerMessage = document.querySelector('#body-footer .content p');
	footerMessage.innerHTML = footerTranslated;
}

document.querySelector('#uk-lang').addEventListener('click', function() {
	if (currentLang != 'uk' && !eventChange) translate('uk');
});

document.querySelector('#pt-lang').addEventListener('click', function() {
	if (currentLang != 'pt' && !eventChange) translate('pt');
});

document.querySelector('#linkedin-link').addEventListener('click', function() {
	window.open('https://www.linkedin.com/in/fverdasca/');
});

function errorRequest() {
	document.querySelector('.screen-error').classList.add('show');
	document.querySelector('.aside-timeline').classList.add('hide');
	document.querySelector('.section-main').classList.add('hide');
	document.querySelector('.aside-attributes').classList.add('hide');
}

//initializating project

let request = new XMLHttpRequest();
request.open('GET', '../json/translations.json', true);
request.onload = function() {
	if (request.status === 200) {
		//store info retrieved
		translations = JSON.parse(request.responseText);
		myEvents = translations.eventsPT;
		mySections = translations.sectionsPT;
		//start application after storing info
		addEventsToHtml();
		changeIndex(0);
	} else {
		errorRequest();
	}
};
request.onerror = function() {
	errorRequest();
};

request.send();
